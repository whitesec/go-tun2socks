package v2ray

import (
	// The following are necessary as they register handlers in their init functions.

	// Required features. Can't remove unless there is replacements.
	_ "gitee.com/whitesec/v2ray-core/app/dispatcher"
	_ "gitee.com/whitesec/v2ray-core/app/proxyman/inbound"
	_ "gitee.com/whitesec/v2ray-core/app/proxyman/outbound"

	// Default commander and all its services. This is an optional feature.
	// _ "gitee.com/whitesec/v2ray-core/app/commander"
	// _ "gitee.com/whitesec/v2ray-core/app/log/command"
	// _ "gitee.com/whitesec/v2ray-core/app/proxyman/command"
	// _ "gitee.com/whitesec/v2ray-core/app/stats/command"

	// Other optional features.
	_ "gitee.com/whitesec/v2ray-core/app/dns"
	_ "gitee.com/whitesec/v2ray-core/app/log"
	_ "gitee.com/whitesec/v2ray-core/app/policy"
	_ "gitee.com/whitesec/v2ray-core/app/router"
	_ "gitee.com/whitesec/v2ray-core/app/stats"

	// Inbound and outbound proxies.
	_ "gitee.com/whitesec/v2ray-core/proxy/blackhole"
	_ "gitee.com/whitesec/v2ray-core/proxy/dokodemo"
	_ "gitee.com/whitesec/v2ray-core/proxy/freedom"
	_ "gitee.com/whitesec/v2ray-core/proxy/http"
	_ "gitee.com/whitesec/v2ray-core/proxy/mtproto"
	_ "gitee.com/whitesec/v2ray-core/proxy/shadowsocks"
	_ "gitee.com/whitesec/v2ray-core/proxy/socks"
	_ "gitee.com/whitesec/v2ray-core/proxy/vmess/inbound"
	_ "gitee.com/whitesec/v2ray-core/proxy/vmess/outbound"

	// Transports
	_ "gitee.com/whitesec/v2ray-core/transport/internet/domainsocket"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/http"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/kcp"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/tcp"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/tls"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/udp"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/websocket"

	// Transport headers
	_ "gitee.com/whitesec/v2ray-core/transport/internet/headers/http"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/headers/noop"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/headers/srtp"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/headers/tls"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/headers/utp"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/headers/wechat"
	_ "gitee.com/whitesec/v2ray-core/transport/internet/headers/wireguard"

	// JSON config support. Choose only one from the two below.
	// The following line loads JSON from v2ctl
	// _ "gitee.com/whitesec/v2ray-core/main/json"
	// The following line loads JSON internally
	_ "gitee.com/whitesec/v2ray-core/main/jsonem"
	// Load config from file or http(s)
	// _ "gitee.com/whitesec/v2ray-core/main/confloader/external"
)
