// +build !ios,!android

package v2ray

import (
	_ "gitee.com/whitesec/v2ray-core/app/commander"
	_ "gitee.com/whitesec/v2ray-core/app/log/command"
	_ "gitee.com/whitesec/v2ray-core/app/proxyman/command"
	_ "gitee.com/whitesec/v2ray-core/app/stats/command"

	_ "gitee.com/whitesec/v2ray-core/transport/internet/domainsocket"
)
